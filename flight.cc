#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "flight.h"


int main()
{
	test testing;
		
	FILE *in = NULL;
	FILE *out = NULL;
	int choice;
	int input[20];
	int output = 0;
	int count = 0;
	int count1 = 0;
	int check = 0;
	int check1 = 0;
	int check2 = 0;
	int check3 = 0;
	int check4 = 0;
	int check5 = 0;
	int check6 = 0;
	int check7 = 0;
	int ran = 0;
	int fin = 0;
	
	in = fopen("flight.txt", "r");
	out = fopen("flight2.txt", "w");
	while(fscanf(in, "%d\n", &choice) != EOF)
	{
		input[count] = choice;
		count++;
	}
	while (fin != 1)
	{
		fprintf(stdout, "Checking Exhaust Door.....\n");
		if (input[0] == 0)
		{
			fprintf(stdout, "Door is Ajar!\n");
			fprintf(stdout, "Attempting to Close Door!\n");
			fprintf(stdout, "Door Closed Properly!\n");
			check = 1;
			fprintf(out, "%d\n", check);
		}
		else
		{
			fprintf(stdout, "Door is Closed!\n");
			check = 1;
			fprintf(out, "%d\n", check);
		}
		fprintf(stdout, "Checking Left Speed Brake.....\n");
		if (input[1] == 0)
		{
			fprintf(stdout, "Left Speed Brake is up!\n");
			fprintf(stdout, "Attempting to put Brake Down!\n");
			fprintf(stdout, "Brake Came Down Properly!\n");
			check1 = 1;
			fprintf(out, "%d\n", check1);
		}
		else
		{
			fprintf(stdout, "Brake is Down!\n");
			check1 = 1;
			fprintf(out, "%d\n", check1);
		}
		fprintf(stdout, "Checking Right Speed Brake.....\n");
		if (input[2] == 0)
		{
			fprintf(stdout, "Right Speed Brake is up!\n");
			fprintf(stdout, "Attempting to put Brake Down!\n");
			fprintf(stdout, "Brake Came Down Properly!\n");
			check2 = 1;
			fprintf(out, "%d\n", check2);
		}
		else
		{
			fprintf(stdout, "Brake is Down!\n");
			check2 = 1;
			fprintf(out, "%d\n", check2);
		}
		fprintf(stdout, "Checking Landing Gear.....\n");
		if (input[3] == 0)
		{
			fprintf(stdout, "Landing Gear is up!\n");
			fprintf(stdout, "Attempting to put Landing Gear Down!\n");
			fprintf(stdout, "Landing Gear Came Down Properly!\n");
			check3 = 1;
			fprintf(out, "%d\n", check3);
		}
		else
		{
			fprintf(stdout, "Landing Gear is Down!\n");
			check3 = 1;
			fprintf(out, "%d\n", check3);
		}
		fprintf(stdout, "Checking Flaps.....\n");
		if (input[4] < 15)
		{
			fprintf(stdout, "Flaps are not All the Way Down!\n");
			fprintf(stdout, "Attempting to put Flaps Down!\n");
			output = testing.check(input[4]);
			if (output < 15)
			{
				fprintf(stdout, "Flaps are not Working Try Again!\n");
				check4 = output;
				fprintf(out, "%d\n", check4);
				check5 = 0;
				fprintf(out, "%d\n", check5);
			}
			else
			{
				fprintf(stdout, "Flaps are Down Properly\n");
				check4 = 15;
				fprintf(out, "%d\n", check4);
				check5 = 1;
				fprintf(out, "%d\n", check5);
			}
		}
		else
		{
			fprintf(stdout, "Flaps are Down Properly\n");
			check4 = 15;
			fprintf(out, "%d\n", check4);
			check5 = 1;
			fprintf(out, "%d\n", check5);
		}
		fprintf(stdout, "Checking Throttle.....\n");
		if (input[6] < 15)
		{
			fprintf(stdout, "Throttle not All the Way up!\n");
			fprintf(stdout, "Attempting to put Throttle Up!\n");
			output = testing.check(input[6]);
			if (output < 15)
			{
				fprintf(stdout, "Throttle not Working Try Again!\n");
				check6 = output;
				fprintf(out, "%d\n", check6);
				check7 = 0;
				fprintf(out, "%d\n", check7);
			}
			else
			{
				fprintf(stdout, "Throttle is working\n");
				check6 = 15;
				fprintf(out, "%d\n", check6);
				check7 = 1;
				fprintf(out, "%d\n", check7);
			}
		}
		check = testing.andd(0, 1, check, check1);
		check = testing.andd(0, 1, check, check2);
		check = testing.andd(0, 1, check, check3);
		check = testing.andd(0, 1, check, check4);
		check = testing.andd(0, 1, check, check5);
		check = testing.andd(0, 1, check, check6);
		check = testing.andd(0, 1, check, check7);
		fin = check;
	}
	fclose(in);	
	fclose(out);	

	return(0);
}
