#include <stdio.h>
#include <stdlib.h>

int main()
{
	int count = 0;

	for (count = 0; count < 101; count++)
	{
		if ((count % 3) == 0)
		{
			if ((count % 5) != 0)
			{
				fprintf(stdout, "fizz\n");
			}
			else
			{
				fprintf(stdout, "fizz buzz\n");
			}
		}
		else
		{
			if ((count % 5) == 0)
			{
				fprintf(stdout, "bozz\n");
			}
			else
			{
				fprintf(stdout, "%d\n", count);
			}
		}
	}

	return (0);
}
