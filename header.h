#ifndef _HEADER_H
#define _HEADER_H
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

class states
{
	public:	
	char *andd(char *, char *, char *, char *);
	char *nott(char *, char *, char *);	
	char *nandd(char *, char *, char *, char *);
	char *orr(char *, char *, char *, char *);
	char *norr(char *, char *, char *, char *);
	char *xorr(char *, char *, char *, char *);
	char *nxorr(char *, char *, char *, char *);
	char *latch(char *, char *, char *, char *, char *, char *, int, int);
	char *enlatch(char *, char *, char *, char *, char *, char *, char *, int, int);
	char *msff(char *, char *, char *, char *, char *, char *, char *, char *, char *, int, int);
	char *andor(char *, char *, char *, char *, char *, char *);
	char *aoff(char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, int, int);
	char *decode(char *, char *, char *, char *, int);
};

#endif
