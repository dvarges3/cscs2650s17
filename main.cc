#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"


int main()
{
	states andornot;
	
	FILE *in = NULL;
	FILE *out = NULL;
	FILE *input = NULL;
	char right[11];
	char wrong[11];
	char choice[11];
	char choice2[11];
	char inputs[20][11];
	char inputs2[20][11];
	char outputs[20][11];
	char outputs1[20][11];
	char outputs2[20][11];
	char outputs3[20][11];
	char outputs4[20][11];
	char outputs5[20][11];
	char outputs6[20][11];
	char *string;
	int count = 0;
	int count1 = 0;
	
	string = (char*) malloc(11 * sizeof(char));	
	in = fopen("Logic_Inputs.text", "r");
	out = fopen("profoth.txt", "w");
	input = fopen("test.txt", "r");
	fscanf(in, "%s\n", right);
	fscanf(in, "%s\n", wrong);
	fprintf(out, "%s\n", right);
	fprintf(out, "%s\n", wrong);
	while(fscanf(input, "%s %s\n", &choice, &choice2) != EOF)
	{
		strcpy(inputs[count], choice);
		strcpy(inputs2[count], choice2);
		count++;
	}
	fprintf(stdout, "Outputs for and\n");
	for (count1 = 0; count1 < count; count1++)
	{
		fprintf(stdout, "%s and %s = ", inputs[count1], inputs2[count1]);
		string = andornot.andd(wrong, right, inputs[count1], inputs2[count1]);
		strcpy(outputs[count1], string);
		fprintf(stdout, "%s\n", outputs[count1]);
	}
	fprintf(stdout, "Outputs for nand\n");
	for (count1 = 0; count1 < count; count1++)
	{
		string = andornot.nandd(wrong, right, inputs[count1], inputs2[count1]);
		strcpy(outputs1[count1], string);
		fprintf(stdout, "%s\n", outputs1[count1]);
	}
	fprintf(stdout, "Outputs for or\n");
	for (count1 = 0; count1 < count; count1++)
	{
		fprintf(stdout, "%s and %s = ", inputs[count1], inputs2[count1]);
		string = andornot.orr(wrong, right, inputs[count1], inputs2[count1]);
		strcpy(outputs2[count1], string);
		fprintf(stdout, "%s\n", outputs2[count1]);
	}
	fprintf(stdout, "Outputs for nor\n");
	for (count1 = 0; count1 < count; count1++)
	{
		string = andornot.norr(wrong, right, inputs[count1], inputs2[count1]);
		strcpy(outputs3[count1], string);
		fprintf(stdout, "%s\n", outputs3[count1]);
	}
	fprintf(stdout, "Outputs for xor\n");
	for (count1 = 0; count1 < count; count1++)
	{
		fprintf(stdout, "%s and %s = ", inputs[count1], inputs2[count1]);
		string = andornot.xorr(wrong, right, inputs[count1], inputs2[count1]);
		strcpy(outputs4[count1], string);
		fprintf(stdout, "%s\n", outputs4[count1]);
	}
	fprintf(stdout, "Outputs for nxor\n");
	for (count1 = 0; count1 < count; count1++)
	{
		string = andornot.nxorr(wrong, right, inputs[count1], inputs2[count1]);
		strcpy(outputs5[count1], string);
		fprintf(stdout, "%s\n", outputs5[count1]);
	}
	fprintf(stdout, "Outputs for not\n");
	for (count1 = 0; count1 < count; count1++)
	{
		fprintf(stdout, "%s not ", inputs[count1]);
		string = andornot.nott(wrong, right, inputs[count1]);
		fprintf(stdout, "%s\n", string);
		fprintf(stdout, "%s not ", inputs[count1]);
		string = andornot.nott(wrong, right, inputs2[count1]);
		fprintf(stdout, "%s\n", string);
	}
	fclose(in);
	fclose(out);
	fclose(input);	


	return(0);
}
