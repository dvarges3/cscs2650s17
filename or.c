#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main ()
{
	FILE *in = NULL;
	FILE *source = NULL;
	FILE *out = NULL;
	char *basename = {"Logic_Inputs.text"};
	char right[11];
	char wrong[11];
	char cmp = 0;
	char cmp2 = 0;
	char count = 0;
	char count1 = 0;
	char choice[11];

	in = fopen(basename, "r");
	source = fopen("test.txt", "r");
	if (in == NULL)
	{
		fprintf(stderr, "Error opening '%s' for reading.\n", basename);
		exit(1);
	}
	if (source == NULL)
	{
		fprintf(stderr, "Error opening source for reading.\n");
		exit(1);
	}
	fscanf(in, "%s\n", right);
	fscanf(in, "%s\n", wrong);
	out = fopen("prefot.txt", "w");
	fprintf(out, "%s\n", right);
	fprintf(out, "%s\n", wrong);
	while(fscanf(source, "%s", &choice) != EOF)
	{
		cmp = strcmp(choice, right);
		if (count == 0)
		{
			if (cmp == 0)
			{
				fprintf(stdout, "%s ", choice);
				count1 = 1;
			}
			else
			{
				fprintf(stdout, "%s ", choice);
			}
		}
		if (count == 1)
		{
			if (cmp == 0)
			{
				fprintf(stdout, "%s equals %s\n", choice, right);
				if (count1 == 1)
				{
					count1 = 0;
				}
			}
			else
			{
				fprintf(stdout, "%s equals", choice);
				if (count1 == 1)
				{
					fprintf(stdout, " %s\n", right);
					count1 = 0;
				}
				else
				{
					fprintf(stdout, " %s\n", wrong);
				}
			}
			count-=2;
		}
		count++;
	}
	fclose(out);
	fclose(in);
	fclose(source);
	return 0;
}
